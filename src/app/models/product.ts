export class Product {
    uid?: string;
    productID?:string;
    productName?:string;
    price?:number;
    currency?: string;
    sizes?: string[];
    colors?:string[];
    description?:string;
    image?: string;
    dateAdded?:number;
    isTopBrand?:boolean;
    prodCategory?:string;
}