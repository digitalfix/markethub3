export class Merchant {
    uid?:string;
    businessName?:string;
    bizPhoneNumber?:string;
    siteSubdomain?:string;
    bizAdress?:string;
    bizCategory?:string;
    bizState?:string;
    bizLGA?:string;
    bizStartDate?:number;
    email?:string;
    photo?:string;
}