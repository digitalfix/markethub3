import { Component, OnInit } from '@angular/core';
import {User,UserSession} from '../../models/user';
import {Router,ActivatedRoute} from '@angular/router';
import {OnlineproviderService} from '../../services/onlineprovider/onlineprovider.service';
import {DatasharingService} from '../../services/datasharing/datasharing.service';
import { Merchant } from '../../models/merchant';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
navLocation:number=0;
userInputs:User={};
mySession:UserSession={};
merchantDet:Merchant={};
trialPassword:string;
signedUp:boolean=false;
  constructor(
    private router:Router,
    private backApi:OnlineproviderService,
    private dataSharer:DatasharingService,
    private cookieService:CookieService
  ) { 
  }

  ngOnInit() {
  }


  navToNext(){
    if(this.navLocation!=2){
        this.navLocation++;
        console.log("val is now "+this.navLocation);
    }
      
  }

  navToPrev(){
    if(this.navLocation!=0){
        this.navLocation--;
        console.log("val is now "+this.navLocation);
    }
    
  }


  signUp(){
    this.userInputs.email = this.merchantDet.email;
    if(this.trialPassword!=this.userInputs.password){
      alert("Passwords Do not match");
      return;
    }
    this.backApi.signUp(this.userInputs)
    .then(resp=>{
      var respObj:any=resp;
      if(respObj.message!="user created!"){
        alert(respObj.message);
        this.signedUp = false;
      }else{
        alert(respObj.message);//to be replaced with modal
        this.signedUp = true;
        return respObj;
      }
    })
    .then(res=>{
      if(this.signedUp){
        this.mySession = this.userInputs;
        this.merchantDet.uid = res.data.uid;
        this.backApi.addStore(this.merchantDet).then(resp=>{
            console.log(JSON.stringify(resp));
            var storeObj:any=resp;
            this.mySession.isUserLoggedIn = true;
            this.cookieService.set( 'sessionID', res.data.uid);
            this.cookieService.set( 'storeID', storeObj.data.merchantID);
            this.cookieService.set( 'isUserLoggedIn', 'true');
            this.dataSharer.userSession.next(this.mySession);
            this.router.navigate(['index']);
            var businessName= this.merchantDet.businessName;
            var mailContent = 
            {
              email:this.userInputs.email,
              mailSubject:'Complete your registration with one click',
              businessName:businessName
            };
              this.backApi.sendAuthMail(mailContent).then(res=>{
                  var respObj2:any=res;
                  console.log(respObj2.message);
              }).catch(er=>{
                console.log("Error is: "+er);
              })
        }).catch(err=>{
            console.log("Error is: "+err);
        });
      }
      
    });
  }
}
