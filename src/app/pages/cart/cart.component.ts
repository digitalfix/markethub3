import { Component, OnInit } from '@angular/core';
import {DatasharingService} from '../../services/datasharing/datasharing.service';
import {UserSession} from '../../models/user';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartInfo:UserSession["cartDetails"] = {uid:'',totalPrice:0,cartItems:[]}
  userSess:UserSession;
  constructor(private dataSharer:DatasharingService) {  
    this.dataSharer.userSession.subscribe( value => {
      this.userSess = value;
      if(this.userSess!=null){
        console.log("He gave: "+JSON.stringify(this.userSess))
        this.cartInfo = this.userSess.cartDetails;
      }else{
          
      }
    }); 
   }

  ngOnInit() {
    
  }

  clearCart(){
    this.cartInfo = {totalPrice:0,cartItems:[]};
    this.userSess.cartDetails = this.cartInfo;
    this.dataSharer.clearCartDetails(this.cartInfo)
    this.dataSharer.userSession.next(this.userSess);
    
  }

}
