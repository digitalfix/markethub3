import { Injectable }     from '@angular/core';
import { CanActivate,Router,ActivatedRoute }    from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import {DatasharingService} from './../datasharing/datasharing.service';
import {UserSession} from './../../models/user';

@Injectable()
export class RouterGaurdService implements CanActivate {
usrSession:UserSession;
  constructor(
    private cookieService: CookieService,
    private router: Router,
    private actvRouter:ActivatedRoute,
    private dataSharer:DatasharingService
  ) {}

  canActivate() {
    console.log("I came from: "+this.router.url);
    if(this.cookieService.check('sessionID')){
        if(this.cookieService.get('sessionID')==''){
          this.dataSharer.userSession.next(null);
          this.router.navigate(['login/']);
        }else{
          this.dataSharer.setSession();
          this.usrSession = this.dataSharer.getSession();
          this.dataSharer.userSession.next(this.usrSession);
        }
    }else{
          this.router.navigate(['login/']);
    }
    return true;
  }
}