import { Injectable, OnInit } from '@angular/core';
import {UserSession} from '../../models/user';
import {BehaviorSubject} from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import {OnlineproviderService} from '../../services/onlineprovider/onlineprovider.service';
@Injectable()
export class DatasharingService implements OnInit{
  private cookieSession:UserSession;
  options = {enableHighAccuracy: true,timeout: 5000,maximumAge: 0 };
  
  public userSession:BehaviorSubject<UserSession>= new BehaviorSubject<UserSession>(this.cookieSession);
  public storeEditMode:BehaviorSubject<boolean>= new BehaviorSubject<boolean>(false);
  public usrLoaction:BehaviorSubject<any>= new BehaviorSubject<any>(this.findMe());

  constructor(
    private cookieServce:CookieService,
    private onLineProv:OnlineproviderService
  ){
      this.setSession();
  }
  ngOnInit(){
    
  }

  destroySession(){
    this.cookieServce.delete('sessionID');
    this.userSession.next(null);
  }

  setSession(){ 
    return this.onLineProv.fetchSession(this.cookieServce.get('sessionID')==''?window.location.hostname:this.cookieServce.get('sessionID')).then(resp=>{
      var resObj:any = resp;
      this.cookieSession = resObj;
      this.userSession.next(this.cookieSession);
    })
  }

  getSession(){
    return this.cookieSession
  }

  clearCartDetails(cartDet){
    var usrID
    if(this.cookieServce.get('sessionID')==''){
      usrID = window.location.hostname
    }else{
      usrID = this.cookieServce.get('sessionID')
    }
    cartDet.uid = usrID
    return this.onLineProv.addToCart(cartDet).then(resp=>{

    })
  }

  success(pos) {
    var crd = pos.coords;
  
    console.log('Your current position is:');
    console.log(`Latitude : ${crd.latitude}`);
    console.log(`Longitude: ${crd.longitude}`);
    console.log(`More or less ${crd.accuracy} meters.`);
    
    return crd.latitude;
  }
  
  error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
    return null;
  }

  findMe() {
    if (navigator.geolocation) {
       return navigator.geolocation.getCurrentPosition(this.success, this.error, this.options)
        //let watchid = navigator.geolocation.watchPosition(this.success, this.error, this.options);
    } else {
      alert("Geolocation is not supported by this browser.");
      return null;
    }
  }
}
