import { TestBed, inject } from '@angular/core/testing';

import { OnlineproviderService } from './onlineprovider.service';

describe('OnlineproviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OnlineproviderService]
    });
  });

  it('should be created', inject([OnlineproviderService], (service: OnlineproviderService) => {
    expect(service).toBeTruthy();
  }));
});
