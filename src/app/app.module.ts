import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import { FormsModule }   from '@angular/forms';
import {OnlineproviderService} from './services/onlineprovider/onlineprovider.service';
import {CommunicatorService} from './services/communicator/communicator.service';
import {HttpModule } from '@angular/http';
import {HttpReqst} from './interface/httprequest'
import {DatasharingService} from './services/datasharing/datasharing.service';


import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { SearchresultComponent } from './pages/searchresult/searchresult.component';
import { HomeComponent } from './pages/home/home.component';
import {StoreComponent} from './pages/store/store.component';
import {ProductsComponent} from './pages/products/products.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { ContactComponent } from './pages/contact/contact.component';
import { StoreEditComponent } from './pages/store-edit/store-edit.component';
import { CartComponent } from './pages/cart/cart.component';
import { StorePreviewComponent } from './pages/store-preview/store-preview/store-preview.component';
import { SellComponent } from './pages/sell/sell.component';
import { AdvertiseComponent } from './pages/advertise/advertise.component';
import { CookieService } from 'ngx-cookie-service';
import { RouterGaurdService} from './services/router-gaurd/router-gaurd.service';
import { StoresComponent } from './pages/stores/stores.component';
// import {ReactiveFormsModule } from '@angular/forms'



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    RecoverComponent,
    SearchresultComponent,
    HomeComponent,
    StoreComponent,
    ProductsComponent,
    ProductDetailComponent,
    ContactComponent,
    StoreEditComponent,
    CartComponent,
    StorePreviewComponent,
    SellComponent,
    AdvertiseComponent,
    StoresComponent
    // ReactiveFormsModule
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAKFWBqlKAGCeS1rMVoaNlwyayu0e0YRes'
    })
  ],
  providers: [
    OnlineproviderService,
    CommunicatorService,
    HttpReqst,
    DatasharingService,
    CookieService,
    RouterGaurdService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
